__author__ = 'zj'
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
def read_clearn(year):
            file=year+'_clean.csv'
            data = pd.read_csv(file,sep=',',thousands=',',low_memory=False)

            ids=data['ID']
            schools=data['INSTNM']
            debt=data['GRAD_DEBT_MDN']
            earns=data['md_earn_wne_p6']

            target=['ID','INSTNM','GRAD_DEBT_MDN','md_earn_wne_p6']
            for t in target:
                        data=data.drop(t,axis=1)
            return data,debt
data,earns=read_clearn('2012')



file1='dictionary.csv'
columns=pd.read_csv(file1,sep=',',low_memory=False)
columns_filter=columns[['NAME OF DATA ELEMENT','VARIABLE NAME']]
mapping={}
for index,row in columns_filter.iterrows():
            mapping[row['VARIABLE NAME']]=row['NAME OF DATA ELEMENT']


from sklearn.svm import LinearSVC
from sklearn.feature_selection import RFE
from sklearn.feature_selection import SelectKBest
from sklearn.linear_model import LogisticRegression
import  sklearn.linear_model as linear_model

columns=data.columns

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectPercentile
model = ExtraTreesClassifier(n_estimators=20)
#model=SelectKBest(k=15)
model.fit(data,earns)
# display the relative importance of each attribute
#print(model.scores_)


data=model.transform(data)


ratio=list(model.feature_importances_)
ratio_c=list(model.feature_importances_)
ratio.sort(reverse=True)
print(ratio)
topten=ratio[0:10]
print(topten)
values=[mapping[columns[ratio_c.index(i)]] for i in topten ]
print([columns[ratio.index(i)] for i in topten ])
for feature in range(0,len(values),1):
            print(values[feature])
print(values)
print(len(model.feature_importances_))
print(data.shape[1])
from sklearn import cross_validation
X_train, X_test, y_train, y_test = cross_validation.train_test_split(data, earns, test_size=0.5, random_state=1)

from sklearn.neighbors import KNeighborsRegressor
from sklearn import svm
#neigh=linear_model.LinearRegression()
#neigh = linear_model.BayesianRidge()
neigh = svm.SVC(kernel='rbf',degree=3)
#neigh=linear_model.ARDRegression()
from sklearn.tree import DecisionTreeRegressor
#neigh=DecisionTreeRegressor(max_depth=10)
from sklearn.neighbors import KNeighborsClassifier
#neigh = KNeighborsClassifier(n_neighbors=5)
#neigh=linear_model.Lasso()
neigh.fit(X_train,y_train)
y=neigh.predict(X_test)
MSE=np.abs(y-y_test)/y_test
list=[]
count=0
print(MSE)
print(y)
print(y_test)
MSE=MSE.tolist()
for i in range(len(MSE)):
            list.append(MSE[i]*100)
            if(abs(MSE[i]*100)<30):
                        count+=1
da=pd.DataFrame(list)
l=da.hist(bins=200)
plt.show()
print(count*1.0/len(list))
print((np.mean((y-y_test)**2)))






# ## Biplot - Leverage `seaborn` package

# In[83]:

