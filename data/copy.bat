@echo off
set result=false
if "%~1" == "" set result=true
if "%~1" == "/help" set result=true
if "%~1" == "/?" set result=true
if "%result%" == "true" (goto Usage)

@echo Install csv files to "%~1"
IF NOT EXIST "%~1" (@echo Directroy Not Found. Please check the path.)

copy CollegeScorecard_Raw_Data "%~1"

exit /b 1

:Usage
@echo Usage: copy "$INSALLATION_PATH" (note: with quotes)