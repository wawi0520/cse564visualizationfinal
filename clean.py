__author__ = 'zj'
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


file='MERGED2011_PP.csv'
data = pd.read_csv(file,sep=',',thousands=',',low_memory=False)

predictions=data[['GRAD_DEBT_MDN','md_earn_wne_p6']]
schools=data[['INSTNM']]

data=data[data['GRAD_DEBT_MDN']!='PrivacySuppressed']
data=data[data['md_earn_wne_p6']!='PrivacySuppressed']
data=data.dropna(subset=['GRAD_DEBT_MDN','md_earn_wne_p6'])
print(data.shape[0])


data=data[data['PREDDEG']>0] # or above

file1='dictionary.csv'
columns=pd.read_csv(file1,sep=',',low_memory=False)
columns_filter=columns[['NAME OF DATA ELEMENT','dev-category','VARIABLE NAME']]
columns=columns[['API data type','VARIABLE NAME']]


columns=columns.dropna()
columns_filter=columns_filter.dropna()

columns=columns[columns['API data type']!='string']
columns=columns[columns['API data type']!='autocomplete']
columns=[v for v in columns['VARIABLE NAME'] if v in data.columns]
data=data[columns]
print(data.shape[1])
list=[]

drop_cols=['school','admissions','academics','repayment','earnings','root']
for c in drop_cols:
            columns_filter=columns_filter[columns_filter['dev-category']!=c]


columns=[v for v in columns_filter['VARIABLE NAME'] if v in data.columns]
data=data[columns]
data=data.dropna(axis=1,how='all')
data=data.dropna(axis=0,how='all')
print(data.shape[0],data.shape[1])



fliter=['DEBT','wne','fsend','RPY']
pridet=[c for c in data.columns if len( [1 for f in fliter if f in c])>0]
data=data.drop(pridet,axis=1)
colu=data.columns

data=data.dropna(axis=1,how='all')
data=data.dropna(axis=0,how='all')

print(data.columns)

print(len(pridet))


'''
list=[]
for index,row in data.iterrows():
            list_row=row.tolist()
            length=len(list_row)
            count=0
            for i in list_row:
                        if i!='PrivacySuppressed' and not np.isnan(float(i)):
                                    count+=1
            list.append(count*1.0/length*100)
            if(count*1.0/length*100<50):data=data.drop(index,axis=0)
print(data.shape[0],data.shape[1])
per=pd.DataFrame(list)
per.hist(bins=100)
plt.show()
'''



list=[]
for col in colu:
            d=data[col]
            length=d.shape[0]
            list_col=data[col].tolist()
            count=0
            sum=0
            for i in list_col:
                        if i!='PrivacySuppressed' and not np.isnan(float(i)):
                                    count+=1
                                    sum+=float(i)
            mean=sum/count
            list_col=[float(i) if i!='PrivacySuppressed' and not np.isnan(float(i)) else mean for i in list_col]
            data[col]=pd.DataFrame(list_col)
            #d=d[d!='PrivacySuppressed']
            list.append(count*1.0/length*100)
            if(count*1.0/length*100<90):data=data.drop(col,axis=1)


print(data.shape[0],data.shape[1])
per=pd.DataFrame(list)
per.hist(bins=100)
plt.show()










data=data.fillna(data.mean(axis=0))
print(data)


col=['GRAD_DEBT_MDN','md_earn_wne_p6']
for c in col:
            d=predictions[c]
            d=d[d!='PrivacySuppressed']
            d=d.dropna()
            d=d.astype(float)
            print(d.shape[0])
            lhist=d.hist(bins=300)
            lhist.get_figure().savefig(c+'.png')
            plt.show()

def processCol(data):
            data=data[data!='PrivacySuppressed']
            data=data.dropna()
            data=data.astype(float)
            return data



data['GRAD_DEBT_MDN']=predictions['GRAD_DEBT_MDN']
data['md_earn_wne_p6']=predictions['md_earn_wne_p6']


data['INSTNM']=schools
#data.to_csv('2012_clean.csv')