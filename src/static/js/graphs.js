queue()
    .defer(d3.json, "/CollegeScorecard/projects")
    .defer(d3.json, "/static/geojson/us-states.json")
    .await(makeGraphs);

var ndx;
var topSchools;
var selectedState;
var selectedYear;
var selectedSchool;
var selectedTopSchools = [];
var minDate;
var maxDate;
var oldusChart;
var oldBarChart1;
var oldLineChart;
var g_StatesJson;
var g_LineChartTitleText;
var yearDim;
var stateDim
var feature1 = "PCTFLOAN";
var feature2 = "PCTFLOAN";
var timerVar;
var slideCounter = 0;
var slideBarParam = [];

function InitSlideBarParams(min, max, interval)
{
    function formatPercent(f){var formatter = d3.format(".2f"); return formatter(f);}
    var numOfPoints = ( max - min + 1 ) / interval;
    var numOfIntervals = numOfPoints - 1;
    var total = 100; // 100 percent
    var percentInterval = total / numOfIntervals;

    for(var i = 0; i < numOfPoints; i++)
    {
        slideBarParam.push({ percent: (i * formatPercent(percentInterval)), value: (min+i)});
    }
}

function GetFeatureNames( myFeature )
{
    return myFeature;
}

function ResetSlider()
{
    var percent = +slideBarParam[slideCounter].percent;
    var value = +slideBarParam[slideCounter].value;
    timeSlider = d3.select(".d3-slider-handle");
    timeSlider.attr("style", function(d){return "left: "+percent+"%";});
}

function TimerOffHandler()
{
    // reset counter
    slideCounter = 0;
    // turn off timer
    d3.selectAll(".play").attr("value", "paused");
    clearTimeout( timerVar );
}

function timerOnHandler() {
    if( slideCounter != (slideBarParam.length) )
    {
        var percent = +slideBarParam[slideCounter].percent;
        var newYear = +slideBarParam[slideCounter].value;
        timeSlider = d3.select(".d3-slider-handle");
        timeSlider.attr("style", function(d){return "left: "+percent+"%";});

        //yearDim.filterAll();
        RenderAll(selectedYear, newYear);

        slideCounter++;
    }

    if( slideCounter == (slideBarParam.length) )
    {
        TimerOffHandler();
    }
}

function makeGraphs(error, projectsJson, statesJson) {
    g_StatesJson = statesJson;
    selectedState = "AL";

    var collegeScorecardProjects = projectsJson['ALL'];
    topSchools = projectsJson['TOP'];
    //var keys = [];
    //for(var k in collegeScorecardProjects[0]) keys.push(k);

    //Clean projectsJson data
    collegeScorecardProjects.forEach(function(d) {
        d["YEAR"] = +d["YEAR"];
        d["PCTFLOAN"] = +d["PCTFLOAN"];
        d["COMP_ORIG_YR2_RT"] = +d["COMP_ORIG_YR2_RT"];
        d["APPL_SCH_PCT_GE2"] = +d["APPL_SCH_PCT_GE2"];
        d["WDRAW_ORIG_YR2_RT"] = +d["WDRAW_ORIG_YR2_RT"];
        d["APPL_SCH_PCT_GE3"] = +d["APPL_SCH_PCT_GE3"];
        d["DEP_INC_AVG"] = +d["DEP_INC_AVG"];
        d["IND_INC_AVG"] = +d["IND_INC_AVG"];
        d["SEPAR_DT_MDN"] = +d["SEPAR_DT_MDN"];
        d["COMP_ORIG_YR6_RT"] = +d["COMP_ORIG_YR6_RT"];
        d["UGDS_ASIAN"] = +d["UGDS_ASIAN"];
        d["md_earn_wne_p6"] = +d["md_earn_wne_p6"];
        d["GRAD_DEBT_MDN"] = +d["GRAD_DEBT_MDN"];
    });
    topSchools.forEach(function(d) {
        d["YEAR"] = +d["YEAR"];
        d["PCTFLOAN"] = +d["PCTFLOAN"];
        d["COMP_ORIG_YR2_RT"] = +d["COMP_ORIG_YR2_RT"];
        d["APPL_SCH_PCT_GE2"] = +d["APPL_SCH_PCT_GE2"];
        d["WDRAW_ORIG_YR2_RT"] = +d["WDRAW_ORIG_YR2_RT"];
        d["APPL_SCH_PCT_GE3"] = +d["APPL_SCH_PCT_GE3"];
        d["DEP_INC_AVG"] = +d["DEP_INC_AVG"];
        d["IND_INC_AVG"] = +d["IND_INC_AVG"];
        d["SEPAR_DT_MDN"] = +d["SEPAR_DT_MDN"];
        d["COMP_ORIG_YR6_RT"] = +d["COMP_ORIG_YR6_RT"];
        d["UGDS_ASIAN"] = +d["UGDS_ASIAN"];
        d["md_earn_wne_p6"] = +d["md_earn_wne_p6"];
        d["GRAD_DEBT_MDN"] = +d["GRAD_DEBT_MDN"];
    });

    g_LineChartTitleText = d3.select(".info-chart-title").text();

    //Create a Crossfilter instance
    ndx = crossfilter(collegeScorecardProjects);
    oldNdx = ndx;

    DrawCharts();

    d3.selectAll(".featureButtion")
        .on("click", function() {
        feature1 = d3.select(this.parentNode).select('input[name="feature"]:checked').node().value;
        /*$.ajax({
            type: "POST",
            url: "/add",
            data: JSON.stringify([{'account_template': '111'}]),
            dataType: "json",
        });
        queue()
            .defer(d3.json, "/add");*/
        //DrawCharts();
        TimerOffHandler();
        ResetSlider();
        RenderAll(selectedYear, minDate);
    });

    d3.selectAll(".play")
        .on("click", function() {
        var status = d3.select(".play").attr("value");
        if( status == "paused" )
        {
            // From paused to play...
            d3.select(this).attr("value", "played");
            timerVar = setInterval(timerOnHandler, 1000);
        }
        else
        {
            // From play to paused...
            TimerOffHandler();
        }
    });
};

function SelectStateHandler(state)
{
    topSchools.forEach(function(d) {
        if( d["YEAR"] == selectedYear && d["STABBR"] == state )
        {
            selectedTopSchools.push( d );
        }
    });
    
    RenderBarChart();
}

function DeselectStateHandler( state )
{
    selectedTopSchools = selectedTopSchools.filter(function (d) {
                      return d.YEAR != selectedYear || d.STABBR != state;
                 });
    RenderBarChart();
}

function SelectSchoolHandler()
{
    var school = topSchools.filter(function (d) {
                        return d.INSTNM == selectedSchool;
                    });
    var schoolndx = crossfilter( school );
    var schoolYearDim = schoolndx.dimension(function(d) {return d["YEAR"]; });

    RenderLineChart( schoolYearDim, school );
}

function RenderLineChart( schoolYearDim, school )
{
    DisableAllEvents();

    var lineChartTitle = d3.select(".info-chart-title");
    var schoolName;
    var titleText;
    if( school.length != 0 )
    {
        titleText = g_LineChartTitleText + "- " + school[0].INSTNM;
    }
    lineChartTitle.text( titleText )

    var featureValues;
    /*if( "TUITION_IN" == feature2 )
    {
        featureValues = schoolYearDim.group().reduceSum(function(d) {
        return d["TUITIONFEE_IN"];
        });
    }
    else if( "TUITION_OUT" == feature2 )
    {
        featureValues = schoolYearDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_OUT"];
        });
    }*/
    featureValues = schoolYearDim.group().reduceSum(function(d) {
            return d[feature2];
        });

    oldLineChart
    .margins({top: 10, right: 10, bottom: 20, left: 40})
    .dimension(schoolYearDim)
    .group(featureValues)
    .transitionDuration(500)
    .elasticY(true)
    .x(d3.time.scale().domain([minDate, maxDate]))
    .xAxis();

    dc.redrawAll();
    EnableAllEvents();
}

function RenderBarChart()
{
    DisableAllEvents();

    var featureNames = GetFeatureNames( feature1 );
    var barChartTitle = d3.select(".bar-chart-title");
    var schoolName;
    var titleText = featureNames + " by schools";
    barChartTitle.text( titleText );

    var topndx = crossfilter( selectedTopSchools );
    var topStateDim = topndx.dimension(function(d) {return d["STABBR"]; });
    var topSchoolDim = topndx.dimension(function(d) {return d["INSTNM"]; });
    //Calculate metrics
    var featureValues;
    /*if( "TUITION_IN" == feature1 )
    {
        featureValues = topSchoolDim.group().reduceSum(function(d) {
        return d["TUITIONFEE_IN"];
        });
    }
    else if( "TUITION_OUT" == feature1 )
    {
        featureValues = topSchoolDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_OUT"];
        });
    }*/
    featureValues = topSchoolDim.group().reduceSum(function(d) {
            return d[feature1];
        });

    oldBarChart1
        .dimension(topStateDim)
        .group(featureValues)
        .elasticX(true).xAxis().ticks(4);

    dc.redrawAll();

    EnableAllEvents();
}

function RenderUSMap(selectedYear)
{
    DisableAllEvents();

    var featureNames = GetFeatureNames( feature1 );
    var usmapChartTitle = d3.select(".usmap-chart-title");
    var schoolName;
    var titleText = featureNames;
    usmapChartTitle.text( titleText );

    // Clear the filter
    yearDim.filterAll();

    yearDim.filterExact(selectedYear);
    //ndx.groupAll();
    //topndx.groupAll();

    var featureValues;
    /*if( "TUITION_IN" == feature1 )
    {
        featureValues = stateDim.group().reduceSum(function(d) {
        return d["TUITIONFEE_IN"];
        });
    }
    else if( "TUITION_OUT" == feature1 )
    {
        featureValues = stateDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_OUT"];
        });
    }*/
    featureValues = stateDim.group().reduceSum(function(d) {
        return d[feature1];
    });

    var max_state = featureValues.top(1)[0].value;
    // Note: we need to use the OLD chart and modify it.
    oldusChart
        .dimension(stateDim)
        .group(featureValues)
        .colors(["#E2F2FF", "#C4E4FF", "#9ED2FF", "#81C5FF", "#6BBAFF", "#51AEFF", "#36A2FF", "#1E96FF", "#0089FF", "#0061B5"])
        .colorDomain([0, max_state])
        .overlayGeoJson(g_StatesJson["features"], "state", function (d) {
            return d.properties.name;
        })
        .title(function (p) {
            return "State: " + p["key"]
                    + "\n"
                    + feature1 + ": " + Math.round(p["value"]);
        });
    dc.redrawAll();


    EnableAllEvents();
}

function RenderAll( oldYear, newYear )
{
    if( oldYear != newYear &&　0 != selectedTopSchools.length )
    {
        selectedTopSchools.forEach( function(d, updatedIndex)
        {
            for( var i = 0; i < topSchools.length; i++ )
            {
                if( topSchools[i]['YEAR'] == newYear && topSchools[i]['INSTNM'] == d['INSTNM'] )
                {
                    selectedTopSchools[updatedIndex] = topSchools[i];
                    break;
                }
            }
        });
    }

    var school = topSchools.filter(function (d) {
                          return d.INSTNM == selectedSchool;
                     });
    var schoolndx = crossfilter( school );
    var schoolYearDim = schoolndx.dimension(function(d) {return d["YEAR"]; });

    selectedYear = newYear;

    RenderBarChart();
    RenderLineChart( schoolYearDim, school );
    RenderUSMap(newYear);
}

function DrawCharts()
{
    //Define Dimensions
    yearDim = ndx.dimension(function(d) { return d["YEAR"]; });

    //Define values (to be used in charts)
    minDate = yearDim.bottom(1)[0]["YEAR"];
    selectedYear = minDate;
    maxDate = yearDim.top(1)[0]["YEAR"];
    
    InitSlideBarParams(minDate, maxDate,1);
    yearDim.filterExact(minDate);
    stateDim = ndx.dimension(function(d) { return d["STABBR"]; });

    /*topSchools.forEach(function(d) {
        if( d["YEAR"] == selectedYear && d["STABBR"] == selectedState )
        {
            selectedTopSchools.push( d );
        }
    });*/
    var topndx = crossfilter( selectedTopSchools );
    var topStateDim = topndx.dimension(function(d) {return d["STABBR"]; });
    var topSchoolDim = topndx.dimension(function(d) {return d["INSTNM"]; });
    //Calculate metrics
    var numProjectsByResourceType = topSchoolDim.group().reduceSum(function(d)
        {
            return d["TUITIONFEE_OUT"];
        });

    var featureValues;
    /*if( "TUITION_IN" == feature1 )
    {
        featureValues = stateDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_IN"];
        });
    }
    else if( "TUITION_OUT" == feature1 )
    {
        featureValues = stateDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_OUT"];
        });
    }*/
    featureValues = stateDim.group().reduceSum(function(d) {
        return d[feature1];
    });
    

    var max_state = featureValues.top(1)[0].value;

    d3.select('#timeSlider').call(d3.slider()
        .axis(
            d3.svg.axis().ticks(maxDate-minDate+1)
                  .tickFormat(function(d){var formatter = d3.format("d"); return formatter(d);})
             )
        .min(minDate).max(maxDate).step(1)
        .on("slide", function(evt, value) {
            TimerOffHandler();
            RenderAll(selectedYear, value);
        }));

    //Charts
    var barChart1 = dc.rowChart("#school-by-state-bar-chart");
    oldBarChart1 = barChart1;
    // Disable crossfilter's selection
    barChart1.filter = function() {};

    barChart1
        .width(300)
        .height(500)
        .dimension(topStateDim)
        .group(numProjectsByResourceType)
        .elasticX(true).xAxis().ticks(4);

    var usChart = dc.geoChoroplethChart("#us-chart");
    oldusChart = usChart;
    usChart.width(1100)
        .height(300)
        .dimension(stateDim)
        .group(featureValues)
        .colors(["#E2F2FF", "#C4E4FF", "#9ED2FF", "#81C5FF", "#6BBAFF", "#51AEFF", "#36A2FF", "#1E96FF", "#0089FF", "#0061B5"])
        .colorDomain([0, max_state])
        .overlayGeoJson(g_StatesJson["features"], "state", function (d) {
            return d.properties.name;
        })
        .projection(d3.geo.albersUsa()
                    .scale(500)
                    .translate([200, 150]))
        .title(function (p) {
            return "State: " + p["key"]
                    + "\n"
                    + feature1 + ": " + Math.round(p["value"]);
        });

    var school;
    var selectedSchool = selectedTopSchools.filter(function (d) {
                          return d.INSTNM != school;
                     });

    var schoolndx = crossfilter( selectedSchool );
    var schoolYearDim = schoolndx.dimension(function(d) {return d["YEAR"]; });
    var featureBySchool;
    /*if( "TUITION_IN" == feature2 )
    {
        featureBySchool = schoolYearDim.group().reduceSum(function(d) {
        return d["TUITIONFEE_IN"];
        });
    }
    else if( "TUITION_OUT" == feature2 )
    {
        featureBySchool = schoolYearDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_OUT"];
        });
    }*/
    featureBySchool = schoolYearDim.group().reduceSum(function(d) {
        return d[feature2];
    });

    var lineChart = dc.lineChart("#info-of-school-bar-chart");
    oldLineChart = lineChart;

    lineChart.width(300)
    .height(250)
    .margins({top: 10, right: 10, bottom: 20, left: 40})
    .dimension(schoolYearDim)
    .group(featureBySchool)
    .transitionDuration(500)
    .elasticY(true)
    .x(d3.time.scale().domain([minDate, maxDate]))
    .xAxis()

    dc.renderAll();

    EnableAllEvents();
}

function EnableAllEvents()
{
    var usMapClicked = d3.selectAll("#us-chart").select("svg").select("g").selectAll("g");
    usMapClicked.on("click", function() {
        var attr = d3.select(this).attr("class");
        var selectedAttr = "selected";
        var deselectedAttr = "deselected";
        // This state is selected
        var res = attr.split(" ");
        // State name
        selectedState = res[1].toUpperCase();
        if( attr.indexOf(deselectedAttr) > -1 )
        {
            DeselectStateHandler( selectedState );
        }
        else if( attr.indexOf(selectedAttr) > -1 )
        {
            SelectStateHandler( selectedState );
        }
        else
        {
            DeselectStateHandler( selectedState );
        }
    });
 
    var barChartClicked = d3.selectAll("#school-by-state-bar-chart").select("svg").select("g").selectAll("rect");
    barChartClicked.on("click", function() {
        selectedSchool = d3.select(this.parentNode).select("text").text();
        SelectSchoolHandler();
    });
}

function DisableAllEvents()
{
    var usMapClicked = d3.selectAll("#us-chart").select("svg").select("g").selectAll("g");
    usMapClicked.on("click", null);
 
    var barChartClicked = d3.selectAll("#school-by-state-bar-chart").select("svg").select("g").selectAll("rect");
    barChartClicked.on("click", null);
}

function ChangeFeature2()
{
    var sel = document.getElementById('feature2Selection');
    feature2 = sel.options[sel.selectedIndex].value;
    var featureValues;
    /*if( "TUITION_IN" == feature2 )
    {
        featureValues = stateDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_IN"];
        });
    }
    else if( "TUITION_OUT" == feature2 )
    {
        featureValues = stateDim.group().reduceSum(function(d) {
            return d["TUITIONFEE_OUT"];
        });
    }*/
    featureValues = stateDim.group().reduceSum(function(d) {
        return d[feature2];
    });
    
    var school = topSchools.filter(function (d) {
                        return d.INSTNM == selectedSchool;
                    });
    var schoolndx = crossfilter( school );
    var schoolYearDim = schoolndx.dimension(function(d) {return d["YEAR"]; });

    RenderLineChart( schoolYearDim, school );
}
