from pymongo import MongoClient

def connect(host, port, dbs_name, collection_name, field = {}, max_entries = {}):
    
    connection = MongoClient( host, port )
    collection = connection[dbs_name][collection_name]
    
    projects = {}
    if not field:
        if not max_entries:
            projects = collection.find()
        else:
            projects = collection.find(limit=max_entries)      
    else:
        if not max_entries:
            projects = collection.find(projection = field)
        else:
            projects = collection.find(projection = field, limit=max_entries)  

    connection.close()
    return projects