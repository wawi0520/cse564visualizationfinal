from flask import Flask
from flask import render_template
import json
from bson import json_util
from bson.json_util import dumps
import numpy as np
import math
from matplotlib import pyplot as plt
import connectDB
import pandas as pd
import numbers

app = Flask(import_name=__name__)

# Global settings for database
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DBS_NAME_1 = 'CollegeScorecard2008'
DBS_NAME_2 = 'CollegeScorecard2009'
DBS_NAME_3 = 'CollegeScorecard2010'
DBS_NAME_4 = 'CollegeScorecard2011'
DBS_NAME_5 = 'CollegeScorecard2012'
COLLECTION_NAME = 'projects'
FIELDS = {'UNITID': True, 'INSTNM': True, 'STABBR': True, 'PCTFLOAN': True, 'COMP_ORIG_YR2_RT': True, 'APPL_SCH_PCT_GE2': True, 'WDRAW_ORIG_YR2_RT': True, 'APPL_SCH_PCT_GE3': True,'DEP_INC_AVG': True, 'IND_INC_AVG': True, 'SEPAR_DT_MDN': True, 'COMP_ORIG_YR6_RT': True, 'UGDS_ASIAN': True, 'md_earn_wne_p6': True, 'GRAD_DEBT_MDN': True, '_id': False}
#MAX_ENTRIES = {}
MAX_ENTRIES = 7000
TOP_SCHOOL_LIST = []

PUBLIC_SCHOOL = 1

@app.route("/add", methods=['GET', 'POST'])
def add():
    print('Received a json data...')

@app.route("/wetsai/project2/")
def index():
    return render_template("index.html")

def InitTopUniversities():
    df = pd.read_csv('../data/TopUniversitiesByStates.csv');
    global TOP_SCHOOL_LIST;
    TOP_SCHOOL_LIST= df.as_matrix(columns=['ID']);

def ComposeData( json_projects, projects, year ):
    allSchoolList = [];
    topSchoolList = [];
    global TOP_SCHOOL_LIST;
    for project in projects:
        #if project['CONTROL'] != PUBLIC_SCHOOL:
        #    continue;

        # Wordaround: add 0 if null
        if not isinstance( project['PCTFLOAN'], numbers.Real ):
            project['PCTFLOAN'] = 0;
        if not isinstance( project['COMP_ORIG_YR2_RT'], numbers.Real ):
            project['COMP_ORIG_YR2_RT'] = 0;
        if not isinstance( project['APPL_SCH_PCT_GE2'], numbers.Real ):
            project['APPL_SCH_PCT_GE2'] = 0;
        if not isinstance( project['WDRAW_ORIG_YR2_RT'], numbers.Real ):
            project['WDRAW_ORIG_YR2_RT'] = 0;
        if not isinstance( project['APPL_SCH_PCT_GE3'], numbers.Real ):
            project['APPL_SCH_PCT_GE3'] = 0;
        if not isinstance( project['DEP_INC_AVG'], numbers.Real ):
            project['DEP_INC_AVG'] = 0;
        if not isinstance( project['IND_INC_AVG'], numbers.Real ):
            project['IND_INC_AVG'] = 0;
        if not isinstance( project['SEPAR_DT_MDN'], numbers.Real ):
            project['SEPAR_DT_MDN'] = 0;
        if not isinstance( project['COMP_ORIG_YR6_RT'], numbers.Real ):
            project['COMP_ORIG_YR6_RT'] = 0;
        if not isinstance( project['UGDS_ASIAN'], numbers.Real ):
            project['UGDS_ASIAN'] = 0;
        if not isinstance( project['md_earn_wne_p6'], numbers.Real ):
            project['md_earn_wne_p6'] = 0;
        if not isinstance( project['GRAD_DEBT_MDN'], numbers.Real ):
            project['GRAD_DEBT_MDN'] = 0;

        # Manually add in year.
        project['YEAR'] = year;
        # Manually add flag (top school)
        if( project['UNITID'] in TOP_SCHOOL_LIST):
            for topschool in json_projects['TOP']:
                if( project['UNITID'] == topschool['UNITID'] ):
                    project['INSTNM'] = topschool['INSTNM'];
            json_projects['TOP'].append(project)
            #print(project['PCTFLOAN'], project['COMP_ORIG_YR2_RT'], project['YEAR'], project['STABBR'], project['INSTNM'])
        json_projects['ALL'].append(project)
        #print(project['PCTFLOAN'], project['YEAR'], project['STABBR'], project['INSTNM'])
    return json_projects;

@app.route("/CollegeScorecard/projects")
def collegescorecard_projects():
    json_projects = dict();
    json_projects['ALL'] = list();
    json_projects['TOP'] = list();

    InitTopUniversities();

    # Read data from the database
    projects = connectDB.connect(host = MONGODB_HOST, port = MONGODB_PORT, dbs_name = DBS_NAME_1, collection_name = COLLECTION_NAME, field = FIELDS, max_entries = MAX_ENTRIES )
    json_projects = ComposeData( json_projects, projects, 2008 );

    # Read data from the database
    projects = connectDB.connect(host = MONGODB_HOST, port = MONGODB_PORT, dbs_name = DBS_NAME_2, collection_name = COLLECTION_NAME, field = FIELDS, max_entries = MAX_ENTRIES )
    json_projects = ComposeData( json_projects, projects, 2009 );

    # Read data from the database
    projects = connectDB.connect(host = MONGODB_HOST, port = MONGODB_PORT, dbs_name = DBS_NAME_3, collection_name = COLLECTION_NAME, field = FIELDS, max_entries = MAX_ENTRIES )
    json_projects = ComposeData( json_projects, projects, 2010 );

    # Read data from the database
    projects = connectDB.connect(host = MONGODB_HOST, port = MONGODB_PORT, dbs_name = DBS_NAME_4, collection_name = COLLECTION_NAME, field = FIELDS, max_entries = MAX_ENTRIES )
    json_projects = ComposeData( json_projects, projects, 2011 );

    # Read data from the database
    projects = connectDB.connect(host = MONGODB_HOST, port = MONGODB_PORT, dbs_name = DBS_NAME_5, collection_name = COLLECTION_NAME, field = FIELDS, max_entries = MAX_ENTRIES )
    json_projects = ComposeData( json_projects, projects, 2012 );

    json_projects = json.dumps(json_projects, default=json_util.default)
    return json_projects


if __name__ == "__main__":
    app.run(host='0.0.0.0',port=51414,debug=True)